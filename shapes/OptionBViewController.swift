//
//  OptionBViewController.swift
//  shapes
//
//  Created by jatin verma on 2019-11-05.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import UIKit
import Particle_SDK
import Foundation
class OptionBViewController: UIViewController {
    
    let USERNAME = "jatin_verma@outlook.com"
    let PASSWORD = "kaur1234"
    
    let DEVICE_ID = "3f003b001047363333343437"
    var myPhoton : ParticleDevice?
    // outlets
     @IBOutlet weak var imageView2: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView2.image = UIImage(named: "triangle")
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        // 1. Initialize the SDK
               ParticleCloud.init()
        
               // 2. Login to your account
               ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
                   if (error != nil) {
                       // Something went wrong!
                       print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                       // Print out more detailed information
                       print(error?.localizedDescription)
                   }
                   else {
                       print("Login success!")

                       // try to get the device
                       self.getDeviceFromCloud()

                   }
               } // end login
    }
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    func subscribeToParticleEvents(){
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToAllEvents(withPrefix: "rotateAngle", handler: {
            (event :ParticleEvent?, error : Error?) in
            if let _ = error{
                print("could not subscribe to events")
            }else {
              //print("got event with data \(event?.data)")
              let choice = (event?.data)!
              print("Angle: \(choice)")
              let rotateAngle = (choice as NSString).floatValue
            //  UIView.animate(withDuration: 3.0, animations: {
              DispatchQueue.main.async {
                  self.imageView2.transform = CGAffineTransform(rotationAngle: CGFloat(rotateAngle))
              }
            }
        })
    }

    
    

}
